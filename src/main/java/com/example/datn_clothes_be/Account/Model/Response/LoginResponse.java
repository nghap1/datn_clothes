package com.example.datn_clothes_be.Account.Model.Response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponse {
    private String token;
    private String tokenType = "Bearer";
    private String username;
    private Collection<? extends GrantedAuthority> role;

    public LoginResponse(String token, String username, Collection<? extends GrantedAuthority> role) {
        this.token = token;
        this.username = username;
        this.role = role;
    }
}
