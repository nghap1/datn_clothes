package com.example.datn_clothes_be.Security;

import com.example.datn_clothes_be.Account.Model.Entity.Account;
import com.example.datn_clothes_be.Role.Model.Entity.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class CustomerDetailService implements UserDetails {

    private Long id;
    private String username;
    private String password;
    private String phone;
    private String email;
    private Integer active;
    private Collection<? extends GrantedAuthority> authorities;

    public static CustomerDetailService build(Account account){

        List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
        List<Role> userRole = (List<Role>) account.getRoles();
        GrantedAuthority grantedAuthority = null;
        for (int i = 0;i<userRole.size();i++)
        {
            grantedAuthority = new SimpleGrantedAuthority(userRole.get(i).getName());
        }

        grantedAuthorityList.add(grantedAuthority);

        return CustomerDetailService.builder()
                .id(account.getId())
                .username(account.getUserName())
                .password(account.getPassword())
                .email(account.getEmail())
                .active(account.getStatus())
                .authorities(grantedAuthorityList)
                .build();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }
}
