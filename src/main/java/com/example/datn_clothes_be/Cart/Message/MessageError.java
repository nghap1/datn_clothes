package com.example.datn_clothes_be.Cart.Message;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MessageError {
    private String code;

    private String content;
}
