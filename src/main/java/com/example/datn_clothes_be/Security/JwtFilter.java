package com.example.datn_clothes_be.Security;

import com.example.datn_clothes_be.Account.Service.AccountService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class JwtFilter extends OncePerRequestFilter {
    @Autowired
    private JwtUtility jwtUtility;
    @Autowired
    private AccountService accountService;

    private AntPathMatcher pathMatcher = new AntPathMatcher();

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        authenticateRequest(request);
        filterChain.doFilter(request,response);
    }

    private void authenticateRequest(HttpServletRequest request){
        try {
            String jwt = parseJwt(request);

            if(jwt != null && jwtUtility.validateJwtToken(jwt))
            {
                String userName = jwtUtility.getUserNameFromJwtToken(jwt);

                UserDetails userDetails = accountService.loadUserByUsername(userName);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails,null,userDetails.getAuthorities()
                );
                //getUserDetail
                Map<String, List<String>> authoMap= new HashMap<>();
                authoMap.put("User", List.of(

                        "/api/cart/get/user","/api/cart/update-quantity/{id}","/api/cart/delete/{id}","/api/cart/sum-total-price","/api/cart/addToCart",
                        "/order/check-out"
                ));
                authoMap.put("Staff", List.of(
                        "/product/get","/product/create","/product/update-new/{idProduct}","/product/delete/{idProduct}",
                        "/category/get","/category/add", "/category/delete/{id}", "/category/update/{id}",
                        "/brand/get","/brand/add","/brand/delete/{id}", "/brand/update/{id}",
                        "/color/get","/color/add","/color/delete/{id}", "/color/update/{id}",
                        "/account/get","/account/create","/account/update/{idAccount}","/account/delete/{idAccount}",
                        "/employee/get","/employee/status/get"
                        ,"/order/create-order","/order/get"

                ));
                authoMap.put("Admin", List.of(
                        "/product/get","/product/create","/product/update-new/{idProduct}","/product/delete/{idProduct}",
                        "/category/get","/category/add", "/category/delete/{id}", "/category/update/{id}",
                        "/brand/get","/brand/add","/brand/delete/{id}", "/brand/update/{id}",
                        "/color/get","/color/add","/color/delete/{id}", "/color/update/{id}",
                        "/account/get","/account/create","/account/update/{idAccount}","/account/delete/{idAccount}",
                        "/employee/get","/employee/status/get"
                        ,"/order/create-order","/order/get"

                ));
                List<String> authPath = authoMap.get(
                        ((GrantedAuthority)authentication.getAuthorities().toArray()[0]).getAuthority());
                String currentPath = request.getRequestURI().substring(request.getContextPath().length());
                if(authPath.stream().anyMatch(path -> pathMatcher.match(path, currentPath))){
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }

        } catch (Exception e) {
            logger.error("Cannot set user authentication: {}", e);
        }
    }


    private String parseJwt(HttpServletRequest request) {
        String headerAuth = request.getHeader("Authorization");

        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
//            System.out.println(headerAuth.substring(7, headerAuth.length()) + "hahaha");
            return headerAuth.substring(7, headerAuth.length());
        }

        return null;
    }
}
