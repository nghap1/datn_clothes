package com.example.datn_clothes_be.Response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseApi {
    private Boolean status;
    private Optional<?> content;
    private String message;
}


