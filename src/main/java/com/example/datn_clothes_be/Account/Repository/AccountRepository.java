package com.example.datn_clothes_be.Account.Repository;

import com.example.datn_clothes_be.Account.Model.Entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.datn_clothes_be.Account.Model.Response.AccountResponse;
import com.example.datn_clothes_be.Account.Model.Response.AccountRoleResponse;


import java.util.List;

@Repository
    public interface AccountRepository extends JpaRepository<Account,Long> {
        Account findAccountByUserName(String userName);
        @Query(value = "SELECT a.id as id,a.user_name as user_name,a.email as email,a.status as status,r.name as name from  account a inner join  account_role ar on a.id = ar.account_id inner join role r on ar.role_id = r.id",nativeQuery = true)
        List<?> listAccountAndRole();


    }
}
