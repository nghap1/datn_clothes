package com.example.datn_clothes_be.Role.Repository;

import com.example.datn_clothes_be.Account.Model.Entity.Account;
import com.example.datn_clothes_be.Role.Model.Entity.Role;
import com.example.shoes_product.Account.Entity.Account;
import com.example.shoes_product.Role.Entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);
    Role findRoleByAccounts(Account account);
}
