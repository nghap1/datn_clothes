package com.example.datn_clothes_be.Account.Mapping;

import com.example.datn_clothes_be.Account.Model.Entity.Account;
import com.example.datn_clothes_be.Account.Model.Entity.RegisterAccount;
import com.example.datn_clothes_be.Account.Model.Request.AccountRequest;
import com.example.datn_clothes_be.Account.Model.Response.AccountResponse;
import org.springframework.security.crypto.bcrypt.BCrypt;

public class AccountMapping {
    public static Account mapAccount(RegisterAccount registerAccount) {
        Account account = new Account();
        account.setEmail(registerAccount.getEmail());
        account.setUserName(registerAccount.getUserName());
        account.setStatus(1);
        String hasPassword = BCrypt.hashpw(registerAccount.getPassword(),BCrypt.gensalt(10));
        account.setPassword(hasPassword);
        return account;
    }

    public static Account mapRequestToEntity(AccountRequest accountRequest) {
        Account account = new Account();
        account.setId(accountRequest.getId());
        account.setEmail(accountRequest.getEmail());
        account.setUserName(accountRequest.getUserName());
        account.setStatus(accountRequest.getStatus());
        account.setAdmin(accountRequest.getAdmin());

        return account;
    }

    public static AccountResponse mapEntityToResponse(Account account) {
        AccountResponse accountResponse = new AccountResponse();
        accountResponse.setId(account.getId());
        accountResponse.setEmail(account.getEmail());
        accountResponse.setUserName(account.getUserName());
        accountResponse.setStatus(account.getStatus());
        accountResponse.setAdmin(account.getAdmin());
        return accountResponse;
    }
}
