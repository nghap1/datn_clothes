package com.example.datn_clothes_be.Cart.Controller;

import com.example.datn_clothes_be.Cart.Model.Entity.CartEntity;
import com.example.datn_clothes_be.Cart.Service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {"http://127.0.0.1:5173", "*"})
@RestController
@RequestMapping("/api/cart")
@RequiredArgsConstructor
public class  CartController {

    private final CartService cartService;


    //    lấy danh sách giỏ hàng theo người dùng đăng nhập
    @GetMapping("/get/user")
    public ResponseEntity<?> getCartByUser() {
        return ResponseEntity.status(HttpStatus.OK).
                body(cartService.getAllByUser());
    }


    // xóa sản phẩm trong giỏ hàng
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCart(@PathVariable("id") Long productId) {
        cartService.delete(productId);
        return ResponseEntity.status(HttpStatus.OK).body("Xóa thành công");
    }


    // cập nhật số lượng trong giỏ hàng
    @PutMapping("/update-quantity/{id}")
    public ResponseEntity<?> updateQuantity(
            @PathVariable("id") Long idProduct,
            @RequestParam("quantity") Integer quantity
    ){
        return ResponseEntity.ok().body(cartService.updateQuantity(idProduct,quantity));
    }


    //lấy thông tin tổng tiền giỏ hàng và số lượng sản phẩn trong giỏ hàng
    @GetMapping("/sum-total-price")
    public ResponseEntity<?> sumTotalPriceAndQuantity(
    ){
        return ResponseEntity.ok().body(cartService.sumTotalPriceAndQuantity());
    }


    // thêm sản phầm vào giỏ hàng
    @PostMapping("/addToCart")
    public ResponseEntity<?> addToCart(@RequestBody CartEntity cartDto) {
        return ResponseEntity.status(HttpStatus.OK).body(cartService.addToCart(cartDto));
    }

}
