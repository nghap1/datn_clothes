package com.example.datn_clothes_be.Account.Model.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginAccount {
    private String userName;
    private String password;
}
