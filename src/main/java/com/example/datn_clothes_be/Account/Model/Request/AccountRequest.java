package com.example.datn_clothes_be.Account.Model.Request;

import lombok.Data;

@Data
public class AccountRequest {
    private Long id;
    private String email;
    private String userName;
    private Integer status;
    private AdminEntity admin;
    private Long roleId;
}
