package com.example.datn_clothes_be.Account.Model.Response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountRoleResponse {
    private Long id;
    private String userName;
    private String email;
    private Integer status;
    private String role;
}
