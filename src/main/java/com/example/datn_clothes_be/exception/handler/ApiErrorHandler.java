package com.example.datn_clothes_be.exception.handler;

import com.example.datn_clothes_be.exception.BadRequestException;
import com.example.datn_clothes_be.exception.NotFoundException;
import com.example.datn_clothes_be.Cart.Model.Response.ServiceResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApiErrorHandler {

    @ExceptionHandler(value = NotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ResponseEntity<ServiceResult> handlerNotFoundException(NotFoundException ex) { //Cart Response ????
        ServiceResult result = new ServiceResult<>();
        result.setStatus(HttpStatus.NOT_FOUND);
        result.setMessage(ex.getMessage());
        result.setData(null);

        return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler(value = BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ServiceResult> handlerBadRequestException(BadRequestException ex) {

        ServiceResult result = new ServiceResult<>();
        result.setStatus(HttpStatus.BAD_REQUEST);
        result.setMessage(ex.getMessage());
        result.setData(null);

        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
    }
}
