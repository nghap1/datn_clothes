package com.example.datn_clothes_be.Account.Service;

import com.example.datn_clothes_be.Account.Model.Entity.LoginAccount;
import com.example.datn_clothes_be.Account.Model.Entity.RegisterAccount;
import com.example.datn_clothes_be.Account.Model.Request.AccountRequest;
import com.example.datn_clothes_be.Response.ResponseApi;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public interface AccountService extends UserDetailsService {
    UserDetails loadUserByUsername(String var1) throws UsernameNotFoundException;

    ResponseApi Register(RegisterAccount registerAccount);

    ResponseEntity<?> Login(LoginAccount loginIn);

    ResponseApi getAccount();

    ResponseApi createAccount(AccountRequest accountRequest);

    ResponseApi updateAccount(Long idAccount, AccountRequest accountRequest);

    void deleteAccount(Long idAccount);
}