package com.example.datn_clothes_be.Cart.Model.Entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "carts")
@Data
public class CartEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String productName;

    private Long price;

    private String Image;

    private Long total;

    private Long accountId;

    private Long productId;

    private Integer quantity;
}
