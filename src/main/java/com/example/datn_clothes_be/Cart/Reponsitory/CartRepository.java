package com.example.datn_clothes_be.Cart.Reponsitory;

import com.example.datn_clothes_be.Cart.Model.Entity.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository  extends JpaRepository<CartEntity, Long> {

    List<CartEntity> findAllByAccountId(Long accountId);


    @Query(value = "select * \n" +
            "from carts c \n" +
            "where c.account_id = :accountId and c.product_id = :productId ", nativeQuery = true)
    CartEntity findAllByAccountIdAndProductId(
            @Param("accountId") Long accountId ,
            @Param("productId") Long productId
    );


    @Query(value = "SELECT SUM(c.total) FROM carts c WHERE c.account_id = :accountId", nativeQuery = true)
    Long sumPrice(@Param("accountId") Long accountId);


    @Query(value = "SELECT COUNT(c.product_id) FROM carts c WHERE c.account_id = :accountId", nativeQuery = true)
    Integer countCart(@Param("accountId") Long accountId);


}
