package com.example.datn_clothes_be.Account.Model.Response;

public class AccountResponse {
    private Long id;
    private String email;
    private String userName;
    private Integer status;
    private String role;
    private AdminEntity admin;
}
