package com.example.datn_clothes_be.Account.Model.Entity;

import com.example.datn_clothes_be.Role.Model.Entity.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.Collection;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "account")

public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(columnDefinition = "NVARCHAR(100)",nullable = false)
    private String email;
    @Column(columnDefinition = "NVARCHAR(100)",nullable = false,unique = true)
    private String userName;
    @Column(columnDefinition = "NVARCHAR(100)",nullable = false)
    private String password;

    private Integer status; // 0:inactive;1:active

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.MERGE ,fetch = FetchType.EAGER)
    @JoinTable(name = "account_role",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Collection<Role> roles;

    @ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.LAZY)
    @JoinColumn(name = "admin_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonManagedReference
    @JsonIgnore
    private AdminEntity employee;


    public void addRole(Role role) {
        if (role != null) {
            if (roles == null) {
                roles = new ArrayList<>();
            }
            roles.add(role);

            // Kiểm tra và khởi tạo danh sách tài khoản của vai trò
            if (role.getAccounts() == null) {
                role.setAccounts(new ArrayList<>());
            }
            role.getAccounts().add(this);
        }
    }
}
