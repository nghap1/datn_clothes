package com.example.datn_clothes_be;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatnClothesBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatnClothesBeApplication.class, args);
    }

}
