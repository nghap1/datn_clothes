package com.example.datn_clothes_be.Response;

public interface StringResponse {
    String OK = "SUCCESS";
    String Account_Not_Found = "Account not found";
    String Pass_Wrong = "Wrong password";
    String Error_Img_empty = "Failed to store empty file.";
    String Error_Img_file = "You can only upload image file.";
}

