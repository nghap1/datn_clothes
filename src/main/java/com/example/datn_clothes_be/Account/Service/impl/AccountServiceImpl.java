package com.example.datn_clothes_be.Account.Service.impl;

import com.example.datn_clothes_be.Account.Mapping.AccountMapping;
import com.example.datn_clothes_be.Account.Model.Entity.Account;
import com.example.datn_clothes_be.Account.Model.Entity.LoginAccount;
import com.example.datn_clothes_be.Account.Model.Entity.RegisterAccount;
import com.example.datn_clothes_be.Account.Model.Request.AccountRequest;
import com.example.datn_clothes_be.Account.Model.Response.AccountRoleResponse;
import com.example.datn_clothes_be.Account.Model.Response.LoginResponse;
import com.example.datn_clothes_be.Account.Repository.AccountRepository;
import com.example.datn_clothes_be.Response.Response;
import com.example.datn_clothes_be.Response.ResponseApi;
import com.example.datn_clothes_be.Response.StringResponse;
import com.example.datn_clothes_be.Role.Model.Entity.Role;
import com.example.datn_clothes_be.Role.Repository.RoleRepository;
import com.example.datn_clothes_be.Security.CustomerDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AccountServiceImpl {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtUtility jwtUtility;

    @Override
    public UserDetails loadUserByUsername(String var1) throws UsernameNotFoundException {
        Account user = this.accountRepository.findAccountByUserName(var1);
        return CustomerDetailService.build(user);
    }

    @Override
    public ResponseApi Register(RegisterAccount registerIn) {
        Account account = accountRepository.findAccountByUserName(registerIn.getUserName());
        if (account == null) {
            Account accounts = AccountMapping.mapAccount(registerIn);
//            Role role = roleRepository.findByName("User");
//            accounts.setRoles(Collections.singleton(role));

            Role role = this.roleRepository.findByName("User");
            accounts.addRole(role);

            accountRepository.save(accounts);
//            Map<String,Object> result = new HashMap<>();
//            String message = "ok";
//            result.put("result",message);
            return new ResponseApi(true, Optional.of(new ArrayList<>()), "Đăng ký thành công");
        } else {
//            Map<String,Object> result = new HashMap<>();
//            String message = "Tai khoan ton tai";
//            result.put("result",message);
//            return result;
            return new ResponseApi(false, Optional.of(new ArrayList()), "Tài khoản đã tồn tại");
        }
    }

    @Override
    public ResponseEntity<?> Login(LoginAccount loginIn) {
        Account account = accountRepository.findAccountByUserName(loginIn.getUserName());
        if (account == null) {
            return Response.badRequest(StringResponse.Account_Not_Found);
        }
        if (BCrypt.checkpw(loginIn.getPassword(), account.getPassword()) == false) {
            return Response.badRequest(StringResponse.Pass_Wrong);
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginIn.getUserName(), loginIn.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtility.generateJwtToken(loginIn.getUserName());

        CustomerDetailService customerDetailService = (CustomerDetailService) authentication.getPrincipal();

        return ResponseEntity.ok().body(
                new LoginResponse(jwt,
                        customerDetailService.getUsername(),
                        customerDetailService.getAuthorities()
                ));

//        return Response.ok(jwt);
    }


    @Override
    public ResponseApi getAccount() {
        List<Account> accountList = accountRepository.findAll();
        List<AccountRoleResponse> accountRoleResponseList = new ArrayList<>();
        for (int i = 0; i <accountList.size() ; i++) {
            Role role = roleRepository.findRoleByAccounts(accountList.get(i));
            AccountRoleResponse accountResponse = new AccountRoleResponse();
            accountResponse.setId(accountList.get(i).getId());
            accountResponse.setUserName(accountList.get(i).getUserName());
            accountResponse.setEmail(accountList.get(i).getEmail());
            accountResponse.setStatus(accountList.get(i).getStatus());
            accountResponse.setRole(role.getName());
            accountRoleResponseList.add(accountResponse);
        }
        return new ResponseApi(true, Optional.of(accountRoleResponseList), "Lấy dữ liệu tài khoản thành công");
    }


    @Override
    public ResponseApi createAccount(AccountRequest accountRequest) {
        try {
            Role role = roleRepository.findByName("Staff");
            if (role == null) {
                return new ResponseApi(true, Optional.of(new ArrayList<>()), "Role khong ton tai");
            } else {
                Account account = AccountMapping.mapRequestToEntity(accountRequest);
                account.setPassword(passwordEncoder.encode("123456aA@"));
                account.setStatus(0);
                account.addRole(role);
                return new ResponseApi(true, Optional.of(this.accountRepository.save(account)), "Thêm tài khoản thành công");
            }
        } catch (Exception e) {
            return new ResponseApi(false, Optional.of(new ArrayList()), "Lỗi : " + e.getMessage());
        }
    }


    @Override
    public ResponseApi updateAccount(Long idAccount, AccountRequest accountRequest) {
        try {
            Account accountFindById = this.accountRepository.findById(idAccount)
                    .orElseThrow(() -> new NotFoundException(HttpStatus.NOT_FOUND.value(), "Id account not found: " + idAccount));
            Account account = AccountMapping.mapRequestToEntity(accountRequest);
            account.setId(accountFindById.getId());
            account.setPassword(accountFindById.getPassword());

            Role role = roleRepository.findById(accountRequest.getRoleId())
                    .orElseThrow(() -> new NotFoundException(HttpStatus.NOT_FOUND.value(), "Id role not found"));
            account.addRole(role);

            return new ResponseApi(true, Optional.of(this.accountRepository.save(account)), "Cập nhật tài khoản thành công");
        } catch (Exception e) {
            return new ResponseApi(false, Optional.of(new ArrayList()), "Lỗi : " + e.getMessage());
        }
    }

    @Override
    public void deleteAccount(Long idAccount) {

        Role role = roleRepository.findByName("Admin");


        Account accountFindById = this.accountRepository.findById(idAccount)
                .orElseThrow(() -> new NotFoundException(HttpStatus.NOT_FOUND.value(), "Id Account not found: " + idAccount));


        this.accountRepository.deleteById(idAccount);
    }
}
