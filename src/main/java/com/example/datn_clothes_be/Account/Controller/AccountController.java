package com.example.datn_clothes_be.Account.Controller;

import com.example.datn_clothes_be.Account.Model.Entity.LoginAccount;
import com.example.datn_clothes_be.Account.Model.Entity.RegisterAccount;
import com.example.datn_clothes_be.Account.Model.Request.AccountRequest;
import com.example.datn_clothes_be.Account.Service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
@CrossOrigin("*")
@ResponseBody
public class AccountController {
    @Autowired
    private AccountService accountService;

    @PostMapping(value = "/register")
    private ResponseEntity<?> Register(@RequestBody RegisterAccount registerAccount) {
        return new ResponseEntity<>(accountService.Register(registerAccount), HttpStatus.OK);
    }

    ;

    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@RequestBody LoginAccount loginIn) {
        return accountService.Login(loginIn);
    }

    @GetMapping(value = "/get")
    private ResponseEntity<?> GetAccount() {
        return new ResponseEntity<>(accountService.getAccount(), HttpStatus.OK);
    }

//    @GetMapping(value = "/test")
//    public ResponseEntity<?> test(){
//        return new ResponseEntity<>("ok test thanh cong", HttpStatus.OK);
//    }


    @PostMapping("/create")
    public ResponseEntity<?> createAccount(
            @RequestBody AccountRequest accountRequest
    ) {
        return ResponseEntity.ok().body(this.accountService.createAccount(accountRequest));
    }

    @PutMapping("/update/{idAccount}")
    public ResponseEntity<?> updateAccount(
            @PathVariable("idAccount") Long idAccount,
            @RequestBody AccountRequest accountRequest
    ) {
        return ResponseEntity.ok().body(this.accountService.updateAccount(idAccount, accountRequest));
    }

    @DeleteMapping("/delete/{idAccount}")
    public ResponseEntity<?> deleteAccount(
            @PathVariable("idAccount") Long idAccount
    ) {
        this.accountService.deleteAccount(idAccount);
        return ResponseEntity.ok().body("Xóa thành công");
    }

}