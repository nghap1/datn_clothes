package com.example.datn_clothes_be.Cart.Model.Request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderRequest {

    private String receiverName;
    private String receiverPhone;
    private String receiverAddress;
    private String orderDescription;
    private Double shipPrice;
    private Long paymentId;
    private Long employeeId;
    private Boolean checkShipping;

}
