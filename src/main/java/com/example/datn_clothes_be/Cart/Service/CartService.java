package com.example.datn_clothes_be.Cart.Service;

import com.example.datn_clothes_be.Cart.Model.Entity.CartEntity;
import com.example.datn_clothes_be.Cart.Model.Response.CartResponse;


import java.util.Collection;

public interface CartService {
    Collection<CartEntity> getAllByUser();

    void delete(Long productId);

    CartEntity updateQuantity(Long idProduct, Integer quantity);

    CartResponse sumTotalPriceAndQuantity();

    CartEntity addToCart(CartEntity cartDto);
}
