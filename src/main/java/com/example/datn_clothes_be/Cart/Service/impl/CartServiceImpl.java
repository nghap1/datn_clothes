package com.example.datn_clothes_be.Cart.Service.impl;

import com.example.datn_clothes_be.Account.Repository.AccountRepository;
import com.example.datn_clothes_be.Cart.Model.Entity.CartEntity;
import com.example.datn_clothes_be.Cart.Reponsitory.CartRepository;
import com.example.datn_clothes_be.Cart.Service.CartService;
import com.example.datn_clothes_be.Security.CustomerDetailService;
import com.example.datn_clothes_be.Ultil.CurrentUserUtils;
import com.example.datn_clothes_be.exception.BadRequestException;
import com.example.datn_clothes_be.exception.NotFoundException;
import com.example.datn_clothes_be.Account.Model.Entity.Account;
import com.example.datn_clothes_be.Cart.Model.Response.CartResponse;
//import com.example.datn_clothes_be.Product.Entity.ProductEntity;
//import com.example.shoes_product.Product.Repository.ProductRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {

    private final ProductRepository productRepository;
    private final CartRepository cartRepository;
    private final AccountRepository accountRepository;


    @Override
    public Collection<CartEntity> getAllByUser() {
        CustomerDetailService uDetailService = CurrentUserUtils.getCurrentUserUtils();
        List<CartEntity> list = cartRepository.findAllByAccountId(uDetailService.getId());

        Map<Long, CartEntity> map = list.stream()
                .collect(Collectors.toMap(CartEntity::getId, Function.identity()));

        for (Map.Entry<Long, CartEntity> entry : map.entrySet()) {
            CartEntity cart = entry.getValue();
            Optional<ProductEntity> productEntity = productRepository.findById(entry.getValue().getProductId());
            System.out.println(entry.getKey() + "key của product");
            if(productEntity.isPresent()){
                ProductEntity productOld = productEntity.get();
                cart.setPrice(productOld.getExport_price());
                cart.setProductName(productOld.getProduct_name());
            }

        }
        return map.values();
    }



    @Override
    public void delete(Long productId) {
        CustomerDetailService uDetailService = CurrentUserUtils.getCurrentUserUtils();
        CartEntity cart = cartRepository.findAllByAccountIdAndProductId(uDetailService.getId(), productId);

        if (cart == null) {
            throw new NotFoundException(HttpStatus.NOT_FOUND.value(), "Không tồn tại sản phẩm: " + productId + " trong giỏ hàng");
        }
        cartRepository.deleteById(cart.getId());
    }


    @Override
    public CartEntity updateQuantity(Long idProduct, Integer quantity) {
        CustomerDetailService uDetailService = CurrentUserUtils.getCurrentUserUtils();
        //Su dung database de luu
        CartEntity cart = cartRepository.findAllByAccountIdAndProductId(uDetailService.getId(), idProduct);
        if (cart == null) {
            throw new NotFoundException(HttpStatus.NOT_FOUND.value(), "người dùng chưa có sản phẩm id: " + idProduct + " trong giỏ hàng");
        }
        ProductEntity findQuantity = productRepository.findById(idProduct).get();
        if (findQuantity.getQuantity() < quantity) {
            throw new BadRequestException("Bạn chỉ có thể mua tối đa :" + findQuantity.getQuantity() + " của sản phẩm này");
        }
        cart.setQuantity(quantity);
        cart.setTotal((long) (cart.getPrice() * cart.getQuantity()));
        return cartRepository.save(cart);
    }

    @Override
    public CartResponse sumTotalPriceAndQuantity() {
        CustomerDetailService uDetailService = CurrentUserUtils.getCurrentUserUtils();
        CartResponse cartResponse = new CartResponse();
        cartResponse.setTotalAmount(cartRepository.sumPrice(uDetailService.getId()));
        cartResponse.setQuantityCart(cartRepository.countCart(uDetailService.getId()));
        return cartResponse;
    }

    @Override
    public CartEntity addToCart(CartEntity cartDto) {
        CustomerDetailService uDetailService = CurrentUserUtils.getCurrentUserUtils();

        if (uDetailService == null) {
            throw new BadRequestException("User Không tôn tại");
        } else {//Su dung database de luu
            ProductEntity productEntity = productRepository.findById(cartDto.getProductId())
                    .orElseThrow(() -> new NotFoundException(HttpStatus.NOT_FOUND.value(), "product id not found: " + cartDto.getProductId()));
            CartEntity cart = cartRepository.findAllByAccountIdAndProductId(uDetailService.getId(), cartDto.getProductId()  );
//            List<ImageEntity> imageEntity = imagesRepository.getImageByProduct(cartDto.getProductId());
            Account findByAccount = accountRepository.findById(uDetailService.getId())
                    .orElseThrow(() -> new NotFoundException(HttpStatus.NOT_FOUND.value(), "account id not found: " + cartDto.getAccountId()));

            if (productEntity.getQuantity()<=0){
                throw new BadRequestException("Sản phẩm này đã hết hàng, vui lòng chờ cửa hàng nhập thêm");
            }
            if (cart == null) {
                cart = new CartEntity();
                cart.setAccountId(findByAccount.getId());
                cart.setProductName(productEntity.getProduct_name());
                cart.setProductId(productEntity.getId());
                cart.setImage(productEntity.getImage_url());
                cart.setPrice(productEntity.getExport_price());
                cart.setTotal((long) (productEntity.getExport_price() * 1));
                cart.setQuantity(1);
            } else {//Neu san pham da co trong database tang so luong them 1
                cart.setQuantity(cart.getQuantity() + 1);
                if (productEntity.getQuantity() < cart.getQuantity()) {
                    throw new BadRequestException("Bạn chỉ có thể mua tối đa :" + productEntity.getQuantity() + " của sản phẩm này");
                } else {
                    cart.setTotal((long) (cart.getPrice() * cart.getQuantity()));
                }
            }
            return cartRepository.save(cart);
        }
    }

}
