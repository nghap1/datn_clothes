package com.example.datn_clothes_be.Cart.Model.Response;

import lombok.Data;

@Data
public class CartResponse {
    Long totalAmount;
    Integer quantityCart;
}
